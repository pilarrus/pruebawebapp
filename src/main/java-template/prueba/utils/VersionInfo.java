package prueba;

public interface VersionInfo {
    String version = "${pruebawebapp.version}";
    String timestamp = "${pruebawebapp.timestamp}";
    String hash = "${pruebawebapp.hash}";
}